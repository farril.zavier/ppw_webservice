from django.urls import path
from .views import profile, register, check_email, unsubscribe_email

urlpatterns = [
    path('', profile, name='profile'),
    path('register/', register, name='register'),
    path('check_email/', check_email, name='check_email'),
    path('unsubscribe_email/', unsubscribe_email, name='unsubscribe_email'),
]
