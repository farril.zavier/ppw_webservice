from django import forms

class RegistrationForm(forms.Form):
    email = forms.EmailField(label="E-mail", required=True, max_length=300, widget=forms.EmailInput(attrs={'id': 'email', 'class': 'form-control', 'placeholder': 'example@email.com'}))
    name = forms.CharField(label="Name", required=True, max_length=300, widget=forms.TextInput(attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'John Doe'}))
    password = forms.CharField(label="Password", required=True, max_length=300, widget=forms.PasswordInput(attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Password'}))
