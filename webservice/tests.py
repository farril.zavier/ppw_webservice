from django.test import TestCase, Client
from django.urls import resolve
from .forms import RegistrationForm
from .models import Registration
from .views import profile, register


# Create your tests here.

class UnitTest(TestCase):
    def test_webservice_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_webservice_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)

    def test_webservice_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_webservice_using_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_webservice_has_nav(self):
        response = Client().get('/')
        nav = response.content.decode("utf-8")
        self.assertIn("<nav", nav)

    def test_models_can_create_new_registration(self):
        registration = Registration.objects.create(email="example@gmail.com", name="Anonymous", password="password")
        count = Registration.objects.all().count()
        self.assertEqual(count, 1)

    def test_form_is_valid(self):
        form = RegistrationForm(data={'email': 'example@gmail.com', 'name': 'Anonymous', 'password': 'password'})
        self.assertTrue(form.is_valid())

    def test_can_register(self):
        response = Client().post('/register/', {'email': 'example@gmail.com', 'name': 'Anonymous', 'password': 'password'})
        self.assertEqual(response.status_code, 200)
        json_response = response.json()
        self.assertTrue(json_response['register_status'])

    def test_check_email(self):
        response = Client().post('/register/', {'email': 'example', 'name': 'Anonymous', 'password': 'password'})
        self.assertIn("*Valid email is required", response.content.decode("utf-8"))

    def test_check_email_registered(self):
        Registration.objects.create(name="Anonymous", email="example@gmail.com", password="password")
        response = Client().post('/check_email/', data={'email': "example@gmail.com"})
        response = response.json()
        self.assertTrue(response['email_exists'])

    def test_can_unsubscribe(self):
        register_response = Client().post('/register/', {'email': 'example@gmail.com', 'name': 'Anonymous', 'password': 'password'})
        register_response = register_response.json()
        initial_counter = len(register_response['registrations'])
        unsubscribe_response = Client().post('/unsubscribe_email/', data={'email': "example@gmail.com"})
        unsubscribe_response = unsubscribe_response.json()
        current_counter = len(unsubscribe_response['registrations'])
        self.assertNotEqual(initial_counter, current_counter)
