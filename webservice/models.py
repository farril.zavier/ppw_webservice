from django.db import models

# Create your models here.
class Registration(models.Model):
    email = models.EmailField(max_length=300, primary_key=True, unique=True)
    name = models.CharField(max_length=300)
    password = models.CharField(max_length=300)

    def as_dict(self):
        return {
            'email': self.email,
            'name': self.name,
            'password': self.password,
        }
