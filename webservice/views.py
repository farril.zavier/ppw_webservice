from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from webservice.forms import RegistrationForm
from webservice.models import Registration


# Create your views here.

response = {}

def profile(request):
    response['navItem'] = 'profile'
    return render(request, 'profile.html', response)

def register(request):
    response['navItem'] = 'register'
    form = RegistrationForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        Registration.objects.create(**data)
        register_status = True
        registrations = Registration.objects.all()
        registrations = [obj.as_dict() for obj in registrations]
        return JsonResponse({'register_status': register_status, 'registrations': registrations}, content_type='application/json')
    response['form'] = form
    registrations = Registration.objects.all()
    registrations = [obj.as_dict() for obj in registrations]
    response['registrations'] = registrations
    return render(request, 'register.html', response)

def check_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        email_exists = Registration.objects.filter(pk=email).exists()
        return JsonResponse({'email_exists': email_exists})

@csrf_exempt
def unsubscribe_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        Registration.objects.filter(pk=email).delete()
        unsubscribe_status = True
        registrations = Registration.objects.all()
        registrations = [obj.as_dict() for obj in registrations]
        return JsonResponse({'unsubscribe_status': unsubscribe_status, 'registrations': registrations}, content_type='application/json')
