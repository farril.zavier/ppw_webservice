var unsubscribe = function(email) {
    $.ajax({
        method: "POST",
        url: "/unsubscribe_email/",
        data: {email: email},
        success: function(response) {
            if (response.unsubscribe_status) {
                alert(email + " has been unsubscribed.");
            } else {
                alert("Oops! Please try again!");
            }
            $('.row').empty();
            for (var i = 0; i < response.registrations.length; i++) {
                $('.row').append('<div class="card mx-2 mb-3"><div class="card-body"><h5 class="card-title">' + response.registrations[i].name + '</h5><p class="card-text">' + response.registrations[i].email + '</p><a style="color: white;" class="card-btn btn-unsub btn btn-danger" onclick="unsubscribe(' + "'" + response.registrations[i].email + "'" + ')">Unsubscribe</a><br></div></div>');
            }
        },
        error: function() {
            alert("Could not obtain data from server!");
        }
    });
}

setTimeout(function() {
    $(document).ready(function() {

        $(".se-pre-con").fadeOut("slow");

        $('#btn-register').prop('disabled', true);
        $('#form_name').hide();
        $('#form_email').hide();
        $('#form_email2').hide();
        $('#form_password').hide();
        $('#name').val("");
        $('#email').val("");
        $('#password').val("");

        var inverted = false;
        $("#theme").click(function() {
            if (!inverted) {
                $("html").addClass('inverted');
                $("body").addClass('bg-black');
                inverted = true;
            } else {
                $("html").removeClass('inverted');
                $("body").removeClass('bg-black');
                inverted = false;
            }
        });

        $("#accordion").on('click','.accordion-heading', function (e) {
            if ($(this).hasClass('active')) {
                $(this).next('div').slideUp();
                $(this).removeClass('active');
            } else {
                $("#accordion .panel2").slideUp();
                $("#accordion .accordion-heading").removeClass('active');
                $(this).next('div').stop(true,false).slideDown();
                $(this).addClass('active');
            }
        });

        $('#btn-register').prop('disabled', true);
        var flag = [false, false, false, false];
        $('#email').on('input', function() {
            var input = $(this);
            check(input, 0);
            checkButton();
        });
        $('#name').on('input', function() {
            var input = $(this);
            check(input, 1);
            checkButton();
        });
        $('#password').on('input', function () {
            var input = $(this);
            check(input, 2);
            checkButton();
        });

        var check = function(input, idx) {
            if (idx === 0) {
                var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                var is_data = reg.test(input.val());
                if (is_data) {
                    $('#form_email').hide();
                    checkEmail(input.val());
                    flag[idx] = true;
                    return;
                } else {
                    $('#form_email2').hide();
                }
            } else {
                var is_data = input.val();
            }
            if (is_data) {
                if (idx === 0) {
                    $('#form_email').hide();
                } else if (idx === 1) {
                    $('#form_name').hide();
                } else {
                    $('#form_password').hide();
                }
                flag[idx] = true;
            } else {
                if (idx === 0) {
                    $('#form_email').show();
                } else if (idx === 1) {
                    $('#form_name').show();
                } else {
                    $('#form_password').show();
                }
                flag[idx] = false;
            }
        }

        var checkEmail = function(email) {
            var csrftoken = $("[name=csrfmiddlewaretoken]").val();
            $.ajax({
                method: "POST",
                url: "/check_email/",
                headers: {
                    "X-CSRFToken": csrftoken
                },
                data: {email: email},
                success: function(response) {
                    if (response.email_exists) {
                        $('#form_email2').show();
                        flag[3] = false;
                        checkButton();
                    } else {
                        $('#form_email2').hide();
                        flag[3] = true;
                        checkButton();
                    }
                },
                error: function() {
                    alert("Cannot obtain data from server!");
                }
            });
        }

        var checkButton = function() {
            var button = $('#btn-register');
            for (var x = 0; x < flag.length; x++) {
                if (flag[x] === false) {
                    button.prop('disabled', true);
                    return;
                }
            }
            button.prop('disabled', false);
        }

        $(function() {
            $('form').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: "/register/",
                    data: $('form').serialize(),
                    success: function(response) {
                        if (response.register_status) {
                            alert("Thank you for registering!");
                        } else {
                            alert("Oops! Please try again!");
                        }
                        for (var i = 0; i < flag.length; i++) {
                            flag[i] = false;
                        }
                        $('#btn-register').prop('disabled', true);
                        $('#name').val("");
                        $('#email').val("");
                        $('#password').val("");
                        $('.row').empty();
                        for (var i = 0; i < response.registrations.length; i++) {
                            $('.row').append('<div class="card mx-2 mb-3"><div class="card-body"><h5 class="card-title">' + response.registrations[i].name + '</h5><p class="card-text">' + response.registrations[i].email + '</p><a style="color: white;" class="card-btn btn-unsub btn btn-danger" onclick="unsubscribe(' + "'" + response.registrations[i].email + "'" + ')">Unsubscribe</a><br></div></div>');
                        }
                    },
                    error: function() {
                        alert("Error, cannot connect to server!");
                    }
                });
            });
        });
    });
}, 2000);